package com.example.tefa.ui.student

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tefa.base.BaseActivity
import com.example.tefa.databinding.ActivityListStudentBinding
import com.example.tefa.ui.student.adapter.StudentAdapter
import com.example.tefa.ui.viewmodel.MainViewModel

class ListStudentActivity : BaseActivity() {
    private lateinit var binding: ActivityListStudentBinding
    private lateinit var mainViewModel: MainViewModel
    private var adapter: StudentAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListStudentBinding.inflate(layoutInflater)
        mainViewModel = ViewModelProvider(this)[MainViewModel::class.java]
        initIntent()
        initView()
        initObserver()
        initAction()
        setContentView(binding.root)
    }

    private fun initIntent() {
        mainViewModel.getUserList(1)
    }

    private fun initView() {
        binding.rvUserList.apply {
            layoutManager = LinearLayoutManager(this@ListStudentActivity)
            setHasFixedSize(true)
        }
    }

    private fun initObserver() {
        mainViewModel.isLoading.observe(this) {
            if (it) showLoading() else hideLoading()
        }

        mainViewModel.userList.observe(this) {
            adapter = StudentAdapter(it.toList())
            binding.rvUserList.adapter = adapter
            adapter?.notifyDataSetChanged()
        }
    }

    private fun initAction() {
        binding.ivBack.setOnClickListener {
            onBackPressedDispatcher.onBackPressed()
        }
    }
}