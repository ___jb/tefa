package com.example.tefa.data.remote

import com.example.tefa.data.model.UserItem
import com.example.tefa.data.remote.response.Login
import javax.inject.Inject

class Repository @Inject constructor(private val service: ApiService) {

    suspend fun getUserList(page: Int) : List<UserItem> {
        return service.getUserList(page).data
    }

    suspend fun login(bodyRequest: Login.Request) : Login.Response {
        return service.login(bodyRequest)
    }
}